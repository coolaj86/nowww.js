(function () {
  "use strict";

  var escapeHtml = require('escape-html');

  function nowww(req, res, next) {
    var host = (req.headers.host||'').replace(/^www\./, '');
    var hostname = host.split(':')[0];
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var href = protocol + host + req.url;

    if (host === req.headers.host) {
      if (next) {
        next();
      } else {
        res.end("Not sure what to do...");
      }
      return;
    }

    // Permanent Redirect
    res.statusCode = 301;
    res.setHeader('Location', href);
    // TODO set token (cookie, header, something) to notify browser to notify user about www
    res.write(
      'Redirecting to <a href="' + escapeHtml(href) + '">' + escapeHtml(hostname) + '</a>...'
    );
    res.end();

  }

  function create() {
    return nowww;
  }

  module.exports = create;
}());
